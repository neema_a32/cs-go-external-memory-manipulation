#include "SlimMem.h"
#include "Windows.h"

#include <stdio.h>
#include <string>
#include <iostream>

SlimUtils::SlimMem mem;

DWORD pid;
DWORD m_dwClientDLL;
DWORD m_dwEngineDLL;

using namespace std;

enum class Entity_flags : unsigned int {
	on_ground = (1 << 0),
	ducking = (1 << 1),
	water_jump = (1 << 2),
	on_train = (1 << 3),
	in_rain = (1 << 4),
	frozen = (1 << 5),
	at_controls = (1 << 6),
	client = (1 << 7),
	fake_client = (1 << 8),
	in_water = (1 << 9)
};

enum class Entity_offsets : DWORD {
	flags = 0x100,
	entity_list = 0x4C3C4F4,
	team = 0xF0,
	health = 0xFC,
	bSpotted = 0x939,
	loopdist = 0x10
};

struct GlowObjectDefinition_t
{
	void* pEntity;
	float r;
	float g;
	float b;
	float a;
	uint8_t unk1[16];
	bool m_bRenderWhenOccluded;
	bool m_bRenderWhenUnoccluded;
	bool m_bFullBloom;
	uint8_t unk2[10];
};

enum class Client_offsets : DWORD {
	local_player = 0xC5F89C,
	client_state = 0x589B34,
	force_jump = 0x50DF1B8,
	crossahir = 0xB2C8,
	flash_max_alpha = 0xA304,
	glowObjectManager = 0x517B700,
	glowIndex = 0xA330,
	active_weapon = 0x2EE8,
	my_weapons = 0x2DE8,
	fov = 0x31D8,
	scoped = 0x388E
};

bool trigToggle = false;
bool flashToggle = false;
bool glowToggle = false;
bool fovToggle = false;
bool skinChanged = false;

void bhop() {
	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);
	BYTE flags = mem.Read<DWORD>(dwbase + (DWORD)Entity_offsets::flags);

	if (flags & (unsigned int)Entity_flags::on_ground && GetAsyncKeyState(VK_SPACE)) {

		mem.Write<DWORD>(m_dwClientDLL + (DWORD)Client_offsets::force_jump, 6);
	}
	else {
		mem.Write<DWORD>(m_dwClientDLL + (DWORD)Client_offsets::force_jump, 4);
	}

}

void doRadar() {
	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);
	int tm = mem.Read<int>(dwbase + (DWORD)Entity_offsets::team);

	for (int i = 1; i <= 64; i++) {
		//std::cout << "something" << std::endl;
		int pl = mem.Read<int>(m_dwClientDLL + (DWORD)Entity_offsets::entity_list + (i - 1) * 0x10);
		int pltm = mem.Read<int>(pl + (DWORD)Entity_offsets::team);
		int pldor = mem.Read<int>(pl + 0xE9);

		if (pltm != tm && !pldor) {

			mem.Write<int>(pl + (DWORD)Entity_offsets::bSpotted, 1);
		}
	}
}

void doGlow() {
	GlowObjectDefinition_t glowObjects;
	if (GetAsyncKeyState(VK_MENU)) {
		glowToggle = !glowToggle;
		if (glowToggle) std::cout << " !!! [ GLOW ON ] !!! " << std::endl;
		else std::cout << " ... [ GLOW OFF ] ... " << std::endl;
		Sleep(200);
	}

	if (glowToggle) {

		DWORD glowPointer = mem.Read<DWORD>(m_dwClientDLL + (DWORD)Client_offsets::glowObjectManager);
		DWORD playerBase = mem.Read<DWORD>(m_dwClientDLL + (DWORD)Client_offsets::local_player);

		if (glowPointer == NULL || playerBase == NULL) {
			return;

		}
		int Iteam = mem.Read<int>(playerBase + 0xF0);

		for (int i = 0; i <= 64; i++) {
			DWORD entity = mem.Read<DWORD>(m_dwClientDLL + (DWORD)Entity_offsets::entity_list + (i * 0x10));
			bool dormant = mem.Read<bool>(entity + 0xE9);

			if (!dormant) {
				DWORD Glowindex = mem.Read<DWORD>(entity + (DWORD)Client_offsets::glowIndex);
				int Eteam = mem.Read<int>(entity + 0xF0);
				bool playerspotted = mem.Read<bool>(entity + 0x939);

				if (Glowindex == NULL) {
					continue;
				}

				if (Eteam != Iteam && playerspotted == true) {
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x4), 1.f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x8), 0.15f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0xC), 0.73f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x10), 0.7f);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x24), true);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x25), false);

				}

				else if (Eteam != Iteam && playerspotted == false) {
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x4), 1.f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x8), 0.15f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0xC), 0.73f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x10), 1.f);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x24), true);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x25), false);
				}
				else if (Eteam == Iteam && playerspotted == true) {
					glowObjects.r = 0.f;
					glowObjects.g = 0.76862745098039215686274509803922f;
					glowObjects.b = 0.88627450980392156862745098039216f;
					glowObjects.a = 0.7f;
					glowObjects.m_bRenderWhenOccluded = true;
					glowObjects.m_bRenderWhenUnoccluded = false;
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x4), 0.f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x8), 0.76f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0xC), 0.88f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x10), 0.7f);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x24), true);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x25), false);

				}

				else if (Eteam == Iteam && playerspotted == false) {
					glowObjects.r = 0.f;
					glowObjects.g = 0.76862745098039215686274509803922f;
					glowObjects.b = 0.88627450980392156862745098039216f;
					glowObjects.a = 1.f;
					glowObjects.m_bRenderWhenOccluded = true;
					glowObjects.m_bRenderWhenUnoccluded = false;
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x4), 0.f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x8), 0.76f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0xC), 0.88f);
					mem.Write<float>(glowPointer + ((Glowindex * 0x38) + 0x10), 1.f);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x24), true);
					mem.Write<bool>(glowPointer + ((Glowindex * 0x38) + 0x25), false);
				}
			}
		}
	}
}

//void doFOV() {
//	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);
//
//	if (GetAsyncKeyState(VK_F7)) {
//		fovToggle = !fovToggle;
//		if (fovToggle) std::cout << " !!! [ FOV TOGGLED -> 120 ] !!! " << std::endl;
//		else std::cout << " ... [ FOV RESET -> 106 ] ... " << std::endl;
//		Sleep(200);
//	}
//
//	DWORD defOV = mem.Read<int>(dwbase + (int)Client_offsets::fov);
//
//	//std::cout << defOV << std::endl;
//
//	if (defOV != 120) { // run only once
//		mem.Write<int>(dwbase + (int)Client_offsets::fov, 120);
//	}
//
//	if (fovToggle) {
//		if (defOV != 120) { // run only once
//			mem.Write<int>(dwbase + (int)Client_offsets::fov, 120);
//		}
//
//	}
//	//else if (!fovToggle) {
//	//	if (defOV != 100) { // run only once
//	//		mem.Write<int>(dwbase + (int)Client_offsets::fov, 100);
//	//	}
//	//}
//
//	/* 
//	FOVs are different for every aspect ratio
//				16:9 - 106
//				16:10 - 100
//				4:3 - 90 
//	*/
//}

void flashed() {
	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);

	if (GetAsyncKeyState(VK_F2))
	{
		flashToggle = !flashToggle;
		if (flashToggle) {
			std::cout << " !!! [ NOFLASH ON ] !!!" << std::endl;
			if (mem.Read<float>(dwbase + (DWORD)Client_offsets::flash_max_alpha) > 0.0f) {
				mem.Write<float>(dwbase + (DWORD)Client_offsets::flash_max_alpha, 0.0f);
			}
		}
		else {
			std::cout << " ... [ NOFLASH OFF ] ... " << std::endl;
			if (mem.Read<float>(dwbase + (DWORD)Client_offsets::flash_max_alpha) == 0.0f) {
				mem.Write<float>(dwbase + (DWORD)Client_offsets::flash_max_alpha, 255.0f);
			}
		}
		Sleep(200);
	}
}

void doTrigger() {
	bool is_pressed = GetAsyncKeyState(VK_RIGHT) & 0x8000 != 0;
	


	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);
	while (is_pressed)
	{

		trigToggle = true;

		int LocalPlayer_inCross = mem.Read<int>(dwbase + (DWORD)Client_offsets::crossahir);
		int LocalPlayer_Team = mem.Read<int>(dwbase + (DWORD)Entity_offsets::team);

		DWORD t_EntityBase = mem.Read<DWORD>(m_dwClientDLL + (DWORD)Entity_offsets::entity_list + ((LocalPlayer_inCross - 1) * 0x10));
		int t_EntityTeam = mem.Read<int>(t_EntityBase + (DWORD)Entity_offsets::team);

		bool t_EntityDormant = mem.Read<bool>(t_EntityBase + 0xE9);

		if ((LocalPlayer_inCross > 0 && LocalPlayer_inCross <= 64) && (t_EntityBase != NULL) && (t_EntityTeam != LocalPlayer_Team) && (!t_EntityDormant))
		{
			mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
			Sleep(10);
			mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
			Sleep(10);
		}

		bool innerCheck = GetAsyncKeyState(VK_MENU) & 0x8000 == 0;
		if (!innerCheck) {
			trigToggle = false;
			break;
		}
	}
	
}

void _st()
{
	BYTE flags;
	GlowObjectDefinition_t glowObjects;
	DWORD glowPointer;
	DWORD playerBase;
	DWORD dwbase = mem.Read<DWORD>((DWORD)Client_offsets::local_player + m_dwClientDLL);
	while (true)
	{
		bhop();
		doRadar();
		flashed();
		doTrigger();
		doGlow();
		//doFOV();
	}
}

int main() {

	SetConsoleTitle("CheekySpankers (v1.0)");

	SlimUtils::SlimMem::GetPID(L"csgo.exe", &pid);
	mem.Open(pid, SlimUtils::ProcessAccess::Full);

	const SlimUtils::SlimModule *pClientDll;
	const SlimUtils::SlimModule *pEngineDll;

	pClientDll = mem.GetModule(L"client_panorama.dll");
	pEngineDll = mem.GetModule(L"engine.dll");

	m_dwClientDLL = pClientDll->ptrBase;
	m_dwEngineDLL = pEngineDll->ptrBase;

	std::cout << ">Modules stored" << std::endl;

	while (true) {
		_st();
		
	}
}
